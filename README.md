# GitLab Pipeline that calls Ansible using custom image

blog: https://fabianlee.org/2023/10/14/gitlab-invoking-ansible-from-a-gitlab-pipeline-job/


This GitLab repo contains Ansible configuration files that will be invoked from its .gitlab-ci.yml pipeline.  The Ansible playbooks render a set of templates (including decoded Vault secrets).

The Ansible binaries come from a custom Docker image we built at:
https://gitlab.com/gitlab-pipeline-helpers/docker-python-ansible-user-venv


This project has the files needed for invoking ansible:

* ansible.cfg
* ansible_inventory.ini
* group_vars/
  * all
  * testgroup
* host_vars/
  * localhost
  * secondhost
* roles/
  * testrole/ 
  * secretrole/
* playbooks
  * playbook-testrole.yaml
  * playbook-secretrole.yaml


## Creating Ansible Vault password at shell

```
# dummy password I use for this project and its secrets
# THIS VALUE WOULD BE KNOWN ONLY TO ESSENTIAL PERSONNEL FOR A REAL PROJECT!!!
ANSIBLE_PASSWORD=myfakepass123

# environment variable needed by ansible-vault
export ANSIBLE_VAULT_PASSWORD_FILE=~/.vault_pass
# write password to file
echo $ANSIBLE_PASSWORD > $ANSIBLE_VAULT_PASSWORD_FILE

# add 'mysecret: foosecret'
ansible-vault create myvaultsecret

# show secret
ansible-vault view myvaultsecret
```

## Setup GitLab project for Ansible Vault secrets

Settings > CI/CD > Variables

Create 'ANSIBLE_VAULT_PASSWORD_FILE' of type 'file' (not type variable).  

Set the value to 'myfakepass123'
THIS VALUE WOULD BE KNOWN ONLY TO ESSENTIAL PERSONNEL FOR A REAL PROJECT!!!


